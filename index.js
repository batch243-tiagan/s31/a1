	const http = require('http');
	const url = require('url');
	const port = 3000;

	const server = http.createServer(function (req, res) {

		const reqUrl = url.parse(req.url).pathname;

		    if(reqUrl == "/login"){
		    	res.writeHead(200, { "Content-Type": "text/plain" });
				res.end('You are at the login screen')
		    }else{
				res.writeHead(200, { "Content-Type": "text/plain" });
				res.end('404 Page not Found')
			}
	  })

	server.listen(port);
	console.log(`Server running at localhost:${port}`);